import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home/home.component'
import { LoginPageComponent } from './login/login.component'

const routes: Routes = [
  { path: '',
  redirectTo: 'home',
  pathMatch: 'full'
  },
  { path: 'home', component: HomePageComponent },
  { path: 'login', component: LoginPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
