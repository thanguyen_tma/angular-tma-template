import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomePageComponent implements OnInit {
  title = '';

  // Data for left sidebar
  menu = [
    {
      title: "Menu 1",
      href: "#"
    },
    {
      title: "Menu 2",
      href: "#",
      children: [
        {
          title: 'Menu Child 1',
          href: "#"
        },
        {
          title: "Menu Child 2",
          href: "#"
        }
      ]
    },
    {
      title: "Menu 3",
      href: "#"
    }
  ]

  // Data for header
  headerMenu = [
    {
        title: "Menu 1",
        href: "#"
    },
    {
        title: "Menu 2",
        href: "#"
    },
    {
      title: "Menu 3",
      href: "#"
    },
  ];
  userAction = {
    username: "Nguyen",
    avatarUrl: "https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg",
    actions: [
      {
        title: "My Profile",
        href: "/profile"
      },
      {
        title: "Logout",
        href: "/login"
      }
    ]
  }
  logo = {
    title: 'TMA Solution',
    url: 'https://www.tma.vn/Themes/TMAVN.Theme/Images/TMA-logo1.png'
  };


  // Data for footer
  footerCol1 = {
    title: "TMA Solution",
    content: "Leading software company in Vietnam"
  }

  footerCol2 = {
    title: 'Contact Us',
    children: [
      {
        title: 'Contact 1',
        href: "#"
      },
      {
        title: 'Contact 2',
        href: "#"
      }
    ]
  }

  footerCol3 = {
    title: 'About Us',
    children: [
      {
        title: 'Terms & Conditions',
        href: "#"
      },
      {
        title: 'EULA',
        href: "#"
      }
    ]
  }
  
  constructor() { }

  ngOnInit() {
  }

}
