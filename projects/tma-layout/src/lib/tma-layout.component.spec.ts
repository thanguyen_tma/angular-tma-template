import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmaLayoutComponent } from './tma-layout.component';

describe('TmaLayoutComponent', () => {
  let component: TmaLayoutComponent;
  let fixture: ComponentFixture<TmaLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmaLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmaLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
