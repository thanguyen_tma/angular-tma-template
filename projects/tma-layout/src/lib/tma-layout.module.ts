import { NgModule } from '@angular/core';
import { TmaLayoutComponent } from './tma-layout.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { LeftSideBarComponent } from './left-sidebar/left-sidebar.component';
import { FooterComponent } from './footer/footer.component';

import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [TmaLayoutComponent, LoginComponent, HeaderComponent, LeftSideBarComponent, FooterComponent],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule
  ],
  exports: [TmaLayoutComponent, LoginComponent, HeaderComponent, LeftSideBarComponent, FooterComponent]
})
export class TmaLayoutModule { }
