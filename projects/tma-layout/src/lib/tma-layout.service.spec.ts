import { TestBed } from '@angular/core/testing';

import { TmaLayoutService } from './tma-layout.service';

describe('TmaLayoutService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TmaLayoutService = TestBed.get(TmaLayoutService);
    expect(service).toBeTruthy();
  });
});
