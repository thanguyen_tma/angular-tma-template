
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  submitted = false;
  loginForm: FormGroup;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  get form() { 
    return this.loginForm.controls; 
  }

  onLoginSubmit() {
      this.submitted = true;

      if (this.loginForm.invalid) {
        return;
      }
      alert("Email: " + this.loginForm.value.email + "\nPassword: " + this.loginForm.value.password);
  }

}

