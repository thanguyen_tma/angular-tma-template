import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  @Input() footerCol1: any;
  @Input() footerCol2: any;
  @Input() footerCol3: any;

  constructor() { }

  ngOnInit() {
  }

}
