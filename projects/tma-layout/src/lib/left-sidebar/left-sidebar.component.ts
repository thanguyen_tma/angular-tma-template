import { Component, OnInit, Input } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-left-sidebar',
  templateUrl: './left-sidebar.component.html',
  styleUrls: ['./left-sidebar.component.scss']
})
export class LeftSideBarComponent implements OnInit {
  @Input() userAction: any;
  @Input() menu: Array<any>; 
  @Input() footerFeedBack: any;

  constructor() {

  }

  ngOnInit() {
   
  }

}
