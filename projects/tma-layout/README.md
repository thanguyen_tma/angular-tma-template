# Introduction

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.0.

## How to use?

1. Import lib tma-layout to use.
```javascript
    import { TmaLayoutModule } from 'tma-layout';
    @NgModule({
      declarations: [
        AppComponent
      ],
      imports: [
        BrowserModule,
        AppRoutingModule,
        TmaLayoutModule
      ],
      providers: [],
      bootstrap: [AppComponent]
    })
    export class AppModule { }
```


3. Usage

  ```html
    <app-header [logo]="logo" [menu]="headerMenu" [userAction]="userAction"></app-header>
    <div class="main-content">
        <app-left-sidebar [menu]="menu"></app-left-sidebar>
        <router-outlet></router-outlet>
    </div>
    <app-footer [footerCol1]="footerCol1" [footerCol2]="footerCol2" [footerCol3]="footerCol3"></app-footer>
  ```
3. Parameter values (inputs)

    | Component  | Input | Type | Example |
    | ------------- | ------------- |------------- | ------------- |
    | app-header  | menu | Array | [{title: "Menu 1", href: "#"},{title: "Menu 2", href: "#"}, ...] |
    |  | userActions | Object |  {username: "Nguyen", avatarUrl: "https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg", actions: [{title: "My Profile", href: "/profile"},{title: "Logout", href: "/login"}]} |
    |  | logo | Object | {title: 'TMA Solution', url: 'https://www.tma.vn/Themes/TMAVN.Theme/Images/TMA-logo1.png'} |
    | app-left-sidebar | menu | Array | [{title: "Menu 1", href: "#"},{title: "Menu 2", href: "#", children: [{title: 'Menu Child 1', href: "#", ...]}] |
    | app-login |  |  |  |
    | app-footer | footerCol1 |  |  |
    |  | footerCol2 |  |  |
    |  | footerCol3 |  |  |